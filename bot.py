import threading
import time
import ws

class HardcoreAI:
	def __init__(self):
		self.websocketslib = ws.WebSocketsLayer()

		self.ws = threading.Thread(target=self.websocketslib.connect)
		self.ws.start()

		self.reader = threading.Thread(target=self.read_messages)
		self.reader.start()

	def read_messages(self):
		self.buffer = []
		while True:
			time.sleep(1)
			# are there any new messages?
			try:
				if len(self.buffer)<len(self.websocketslib.relevant_buffer):
					# how many new messages?
					n = len(self.websocketslib.relevant_buffer) - len(self.buffer)
					# get lock
					with self.websocketslib.LOCK:
						# copy whole relevant buffer
						self.buffer = self.websocketslib.relevant_buffer.copy()
					# iterate new messages
					for i in self.buffer[-n:]:
						# process it
						response = self.process(i["text"], i["sender"])
						# do we have an answer?
						if response:
							# send it!
							self.websocketslib.api.send("send message", {"text":response})
			except RuntimeError:
				print("D> AI processing thread dying")
				return False 

	def process(self, message, sender):
		# remove bot's name + ", " from message
		message = message[len(ws._username)+2:]

		# do whatever you want

		if sender == ws._root:
			# pseudo root commands
			if message == "quit":
				self.websocketslib.api.logout()
				return None

		import random
		if random.randint(0,1):
			# message text
			return "Luckyy"

		# we can't respond
		print("D> returning false")
		return False

if __name__=="__main__":
	ai = HardcoreAI()
	ai.reader.join()
	print("D> AI processing thread died.")
	ai.ws.join()
	print("D> WebSockets main thread died.")