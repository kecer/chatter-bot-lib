from socketIO_client import SocketIO, BaseNamespace
import time
import threading

_server = 'kecer.net'
_port = 2357
_username = "randomBot"
_password = "thispasswordisashit"
_firstrun = True
_root = "ping"


def start_thread(target):
	return threading.Thread(target=target).start()

class WebSocketsLayer:
	def __init__(self):
		self.logged = False
		self.LOCK = threading.RLock() # threading lock (for accessing relevant_buffer)
		self.buffer = [] # list of all messages
		self.relevant_buffer = [] # list of relevant messages (starting with _username + ", ", ie "LAIn, blabla")
		self.online = [] # list of online people

	def connect(self):
		try:
			self.socketIO = SocketIO(_server, _port)
			self.api = self.socketIO.define(Namespace, '/api')
			self.api.parent = self
			self.socketIO.wait()
		except:
			print('E> Shuting down.')

class Namespace(BaseNamespace):
	def add_to_buffer(self, data, history=False):
		# if it's not message from history and it starts with our username
		if not history and data["text"].find("{0}, ".format(_username)) == 0:
			with self.parent.LOCK:
				self.parent.relevant_buffer.append(data.copy())
		self.parent.buffer.append(data.copy())

	def update_online(self,data):
		self.parent.online = data.copy()

	def send(self, event, data={}, level=0):
		data["user"] = _username
		print('I> Sending "{0}" request. Payload: {1}'.format(event, data))
		self.emit(event, data)

	def register(self):
		self.send("send command", {"command":"register", "query":_password})

	def keep_alive(self):
		while _logged:
			print(_logged)
			self.send("keep alive", level=-1)
			time.sleep(15)
		print("D> Keep alive thread dying.")

	def logout(self):
		global _logged
		if _logged:
			_logged = False
			self.send("send message", {"text":"Bye bye."})
			self.send("logout")
		self.disconnect()
		print("D> WebSockets main thread dying.")
		del(self.parent.socketIO)
		del(self.parent)
		raise RuntimeError

	def on_connect(self):
		print('I> Connected. Logging in.')
		if _firstrun: self.send("login")
		else: self.send("login", {"password":_password})

	def on_login_status(self, data):
		if "error" in data:
			print('E> Received login error. {0}'.format(data["error"]),3)
			self.logout()
		global _logged
		_logged = True

		self.send("get history")
		self.send("send message",{"text":"{0} is up! Hello.".format(_username)})
		start_thread(self.keep_alive)
		if _firstrun:
			print("W> Looks like this is the first run. If it is not, set _firstrun variable to False.\nI>Attempting to register now.")
			self.register()

	def on_chat(self, data, history=False):
		if not _logged: return False
		self.add_to_buffer(data, history)
		text = data["text"]
		sender = data["sender"]
		if "class" in data and data["class"]:
			print("{0} [{1}]: {2}".format(sender, data["class"], text))
		else:
			print("{0}: {1}".format(sender, text))

	def on_privchat(self, data):
		if not _logged: return False
		self.add_to_buffer(data)
		text = data["text"]
		sender = data["sender"]
		print("[PRIVATE] {0}: {1}".format(sender, text))

	def on_status(self, data):
		if not _logged: return False
		self.add_to_buffer(data)
		text = data["text"]
		sender = ""
		if "sender" in data:
			sender = data["sender"]
		if text=="Password changed.":self.send("send message", {"text":"Registered."})

		print("* {0} {1}".format(sender, text))

	def on_history(self, data):
		for i in data["history"]:
			if i["type"] == "msg":
				self.on_chat(i, history=True)
			elif i["type"] == "status":
				self.on_status(i)
			else:
				print('W> Got strange record from history event. Please, report it.\n\tData: {0}'.format(str(data)))

	def on_online(self, data):
		self.update_online(data)

	def on_kicked(self, data):
		if data["name"] == _username:
			print('E> Got kicked. Reason: {0}'.format(data["reason"]))
			exit()
		print('* {0} got kicked. Reason: {1}'.format(data["name"], data["reason"]))

	def on_report_online(self,data):
		name = data["name"]
		if name not in self.parent.online:
			self.parent.online.append(name)

	def on_report_offline(self,data):
		name = data["name"]
		if name in self.parent.online:
			del(self.parent.online[self.parent.online.index(data["name"])])
		
if __name__=="__main__":
	ws = WebSocketsLayer()